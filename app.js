require('dotenv').config();
const express = require('express');
const app = express();
const {body, query} = require('express-validator/check');
const order = require('./src/order');

app.use(express.json());

app.post('/order', [
    body(['origin', 'destination'], 'Invalid request format')
        .exists()
        .custom((value, { req }) => {
            return Array.isArray(value) && value.length === 2;
        }),
    body(['origin.*', 'destination.*'], 'Values must be real numbers').isFloat(),
], order.create);

app.put('/order/:id', [
    body('status', 'Invalid request format').exists().equals('taken')
], order.take);

app.get('/orders', [
    query(['page', 'limit'], 'Invalid parameters format').exists().isInt({min: 1})
], order.list);

app.listen(8080, () => console.log('Orders API listening on port 8080'));