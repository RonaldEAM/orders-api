const mysql = require('mysql');

var pool = null;

exports.storeOrder = function(origin, destination, distance) {
    return new Promise((resolve, reject) => {
        getPool().query(
            'INSERT INTO `order` (origin_lat, origin_lng, dest_lat, dest_lng, distance) VALUES (?, ?, ?, ?, ?)',
            [origin.lat, origin.lng, destination.lat, destination.lng, distance],
            function (error, results, fields) {
                if (!error) {
                    return resolve(results.insertId);
                } else {
                    reject(new Error('Failed to insert new order'));
                }
            }
        );
    });
}

exports.updateOrderStatus = function(id) {
    return new Promise((resolve, reject) => {
        getPool().query(
            'UPDATE `order` SET status = \'taken\' WHERE id = ? AND status = \'UNASSIGN\'', [id],
            function (error, results, fields) {
                if (!error) {
                    resolve(results.affectedRows === 1);
                } else {
                    reject(new Error('Failed to update order status'));
                }
            }
        );
    });
}

exports.getOrders = function(offset, limit) {
    return new Promise((resolve, reject) => {
        getPool().query(
            'SELECT * FROM `order` LIMIT ?,?',
            [offset, limit],
            function (error, results, fields) {
                if (!error) {
                    resolve(results.map((order) => {
                        return { id: order.id, distance: order.distance, status: order.status };
                    }));
                } else {
                    reject(new Error('Failed to fetch orders'));
                }
            }
        );
    });
}

function getPool() {
    if (pool === null)
        pool = createPool();

    return pool;
}

function createPool() {
    let newPool = mysql.createPool({
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASS,
        database: process.env.DB_NAME
    });

    return newPool;
}