const axios = require('axios');
const {validationResult} = require('express-validator/check');
const database = require('./database');

exports.create = function(req, res) {
    validateRequest(req, res);

    let originPoint = { lat: req.body.origin[0], lng: req.body.origin[1] };
    let destPoint = { lat: req.body.destination[0], lng: req.body.destination[1] };

    let formattedCoords = originPoint.lat + ',' + originPoint.lng + ';' +
        destPoint.lat + ',' + destPoint.lng;

    let url = 'https://api.mapbox.com/directions/v5/mapbox/driving/'
        + encodeURIComponent(formattedCoords)
        + '.json';

    axios.get(url, { params: { access_token: process.env.MAPS_KEY } })
        .then((response) => {
            if (response.data.routes && response.data.routes.length && response.data.routes[0].distance)
                return response.data.routes[0].distance;
            else
                throw new Error('Invalid response from Distance API');
        })
        .then((distance) => {
            return new Promise((resolve, reject) => {
                database.storeOrder(originPoint, destPoint, distance)
                    .then((id) => {
                        resolve({id, distance});
                    })
                    .catch((error) => {
                        reject(error);
                    });
            });
        })
        .then(({id, distance}) => {
            res.json({
                id: id,
                distance: distance,
                status: "UNASSIGN"
            });
        })
        .catch((error) => {
            res.status(500).json({error: error.message});
        });
};

exports.take = function(req, res) {
    validateRequest(req, res);

    database.updateOrderStatus(req.params.id)
        .then((updated) => {
            if (updated) {
                res.json({status: 'SUCCESS'});
            } else {
                res.status(409).json({error: 'ORDER_ALREADY_BEEN_TAKEN'});
            }
        })
        .catch((error) => {
            res.status(500).json({error: error.message});
        });
};

exports.list = function(req, res) {
    validateRequest(req, res);

    let page = parseInt(req.query.page);
    let limit = parseInt(req.query.limit);
    let offset = (page - 1) * limit;

    database.getOrders(offset, limit)
        .then((orders) => {
            res.json(orders);
        })
        .catch((error) => {
            res.status(500).json({error: error.message});
        });
};

function validateRequest(req, res) {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(500).json({ error: errors.array()[0].msg });
    }
}