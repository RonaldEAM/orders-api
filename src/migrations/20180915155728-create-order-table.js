'use strict';

var dbm;
var type;
var seed;

/**
  * We receive the dbmigrate dependency from dbmigrate initially.
  * This enables us to not have to rely on NODE_PATH.
  */
exports.setup = function(options, seedLink) {
  dbm = options.dbmigrate;
  type = dbm.dataType;
  seed = seedLink;
};

exports.up = function(db) {
  return db.createTable('order', {
    id: { type: 'int', primaryKey: true, autoIncrement: true },
    origin_lat: { type: 'real', notNull: true },
    origin_lng: { type: 'real', notNull: true },
    dest_lat: { type: 'real', notNull: true },
    dest_lng: { type: 'real', notNull: true },
    distance: { type: 'real', notNull: true },
    status: { type: 'string', notNull: true, defaultValue: 'UNASSIGN' }
  });
};

exports.down = function(db) {
  return db.dropTable('order');
};

exports._meta = {
  "version": 1
};
