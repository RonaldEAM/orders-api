FROM node:8

WORKDIR /app

ADD . /app

ADD package.json /app
RUN npm install

ADD . /app

RUN cp docker-entrypoint.sh /usr/local/bin/ && \
    chmod +x /usr/local/bin/docker-entrypoint.sh

EXPOSE 8080

ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]