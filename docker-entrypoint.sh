#!/bin/bash

cp .env.example .env
npm run db-migrate up
npm run start